const tokenDecimals = 9;
const contractAddress = "0x0c9D1fcD8f72368CbBe9619f8a398AFD03fC4907";
const contractABI = [{
  "inputs": [{
    "internalType": "address",
    "name": "_stakingToken",
    "type": "address"
  }, {"internalType": "address", "name": "_rewardToken", "type": "address"}],
  "stateMutability": "nonpayable",
  "type": "constructor"
}, {
  "inputs": [{"internalType": "uint256", "name": "attempted", "type": "uint256"}, {
    "internalType": "uint256",
    "name": "available",
    "type": "uint256"
  }], "name": "InsufficientStake", "type": "error"
}, {"inputs": [], "name": "StakingTokenRescue", "type": "error"}, {
  "inputs": [],
  "name": "ZeroAmount",
  "type": "error"
}, {
  "anonymous": false,
  "inputs": [{"indexed": false, "internalType": "address", "name": "owner", "type": "address"}],
  "name": "OwnershipTransferred",
  "type": "event"
}, {
  "anonymous": false,
  "inputs": [{"indexed": false, "internalType": "address", "name": "account", "type": "address"}, {
    "indexed": false,
    "internalType": "uint256",
    "name": "amount",
    "type": "uint256"
  }],
  "name": "Realised",
  "type": "event"
}, {
  "anonymous": false,
  "inputs": [{"indexed": false, "internalType": "address", "name": "account", "type": "address"}, {
    "indexed": false,
    "internalType": "uint256",
    "name": "amount",
    "type": "uint256"
  }],
  "name": "Staked",
  "type": "event"
}, {
  "anonymous": false,
  "inputs": [{"indexed": false, "internalType": "address", "name": "account", "type": "address"}, {
    "indexed": false,
    "internalType": "uint256",
    "name": "amount",
    "type": "uint256"
  }],
  "name": "Unstaked",
  "type": "event"
}, {
  "inputs": [{"internalType": "address", "name": "adr", "type": "address"}],
  "name": "authorize",
  "outputs": [],
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "inputs": [],
  "name": "emergencyUnstakeAll",
  "outputs": [],
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "inputs": [{"internalType": "address", "name": "account", "type": "address"}, {
    "internalType": "uint256",
    "name": "amount",
    "type": "uint256"
  }], "name": "forceUnstake", "outputs": [], "stateMutability": "nonpayable", "type": "function"
}, {
  "inputs": [{"internalType": "uint256", "name": "amount", "type": "uint256"}],
  "name": "getCumulativeRewards",
  "outputs": [{"internalType": "uint256", "name": "", "type": "uint256"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [],
  "name": "getCumulativeRewardsPerToken",
  "outputs": [{"internalType": "uint256", "name": "", "type": "uint256"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [],
  "name": "getLastContractBalance",
  "outputs": [{"internalType": "uint256", "name": "", "type": "uint256"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [{"internalType": "address", "name": "staker", "type": "address"}],
  "name": "getRealisedEarnings",
  "outputs": [{"internalType": "uint256", "name": "", "type": "uint256"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [{"internalType": "address", "name": "account", "type": "address"}],
  "name": "getStake",
  "outputs": [{
    "components": [{
      "internalType": "uint256",
      "name": "amount",
      "type": "uint256"
    }, {"internalType": "uint128", "name": "totalExcluded", "type": "uint128"}, {
      "internalType": "uint128",
      "name": "totalRealised",
      "type": "uint128"
    }], "internalType": "struct MumuBullStaking.Stake", "name": "", "type": "tuple"
  }],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [{"internalType": "address", "name": "account", "type": "address"}],
  "name": "getStakedAmount",
  "outputs": [{"internalType": "uint256", "name": "", "type": "uint256"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [],
  "name": "getTotalRewards",
  "outputs": [{"internalType": "uint256", "name": "", "type": "uint256"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [{"internalType": "address", "name": "staker", "type": "address"}],
  "name": "getUnrealisedEarnings",
  "outputs": [{"internalType": "uint256", "name": "", "type": "uint256"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [{"internalType": "address", "name": "adr", "type": "address"}],
  "name": "isAuthorized",
  "outputs": [{"internalType": "bool", "name": "", "type": "bool"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [{"internalType": "address", "name": "account", "type": "address"}],
  "name": "isOwner",
  "outputs": [{"internalType": "bool", "name": "", "type": "bool"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [],
  "name": "realise",
  "outputs": [],
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "inputs": [{"internalType": "address", "name": "token", "type": "address"}],
  "name": "rescueToken",
  "outputs": [],
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "inputs": [],
  "name": "rewardToken",
  "outputs": [{"internalType": "address", "name": "", "type": "address"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [{"internalType": "address", "name": "reward", "type": "address"}],
  "name": "setRewardToken",
  "outputs": [],
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "inputs": [{"internalType": "uint256", "name": "amount", "type": "uint256"}],
  "name": "stake",
  "outputs": [],
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "inputs": [],
  "name": "stakeAll",
  "outputs": [],
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "inputs": [{"internalType": "address", "name": "staker", "type": "address"}, {
    "internalType": "uint256",
    "name": "amount",
    "type": "uint256"
  }], "name": "stakeFor", "outputs": [], "stateMutability": "nonpayable", "type": "function"
}, {
  "inputs": [{"internalType": "address", "name": "", "type": "address"}],
  "name": "stakes",
  "outputs": [{"internalType": "uint256", "name": "amount", "type": "uint256"}, {
    "internalType": "uint128",
    "name": "totalExcluded",
    "type": "uint128"
  }, {"internalType": "uint128", "name": "totalRealised", "type": "uint128"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [],
  "name": "stakingToken",
  "outputs": [{"internalType": "address", "name": "", "type": "address"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [],
  "name": "totalRealised",
  "outputs": [{"internalType": "uint128", "name": "", "type": "uint128"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [],
  "name": "totalStaked",
  "outputs": [{"internalType": "uint128", "name": "", "type": "uint128"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [{"internalType": "address payable", "name": "adr", "type": "address"}],
  "name": "transferOwnership",
  "outputs": [],
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "inputs": [{"internalType": "address", "name": "adr", "type": "address"}],
  "name": "unauthorize",
  "outputs": [],
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "inputs": [{"internalType": "uint256", "name": "amount", "type": "uint256"}],
  "name": "unstake",
  "outputs": [],
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "inputs": [],
  "name": "unstakeAll",
  "outputs": [],
  "stateMutability": "nonpayable",
  "type": "function"
}, {"inputs": [], "name": "updateRewards", "outputs": [], "stateMutability": "nonpayable", "type": "function"}];
const tokenContractAddress = "0x27e2209f4CeAebed97899eA2d8302Dee690d3820";
const tokenContractABI = [{"inputs": [], "stateMutability": "payable", "type": "constructor"}, {
  "anonymous": false,
  "inputs": [{"indexed": true, "internalType": "address", "name": "owner", "type": "address"}, {
    "indexed": true,
    "internalType": "address",
    "name": "spender",
    "type": "address"
  }, {"indexed": false, "internalType": "uint256", "name": "value", "type": "uint256"}],
  "name": "Approval",
  "type": "event"
}, {
  "anonymous": false,
  "inputs": [{
    "indexed": false,
    "internalType": "uint256",
    "name": "amountCurrency",
    "type": "uint256"
  }, {"indexed": false, "internalType": "uint256", "name": "amountTokens", "type": "uint256"}],
  "name": "AutoLiquify",
  "type": "event"
}, {
  "anonymous": false,
  "inputs": [{"indexed": false, "internalType": "bool", "name": "enabled", "type": "bool"}],
  "name": "ContractSwapEnabledUpdated",
  "type": "event"
}, {
  "anonymous": false,
  "inputs": [{
    "indexed": true,
    "internalType": "address",
    "name": "previousOwner",
    "type": "address"
  }, {"indexed": true, "internalType": "address", "name": "newOwner", "type": "address"}],
  "name": "OwnershipTransferred",
  "type": "event"
}, {
  "anonymous": false,
  "inputs": [{"indexed": true, "internalType": "address", "name": "from", "type": "address"}, {
    "indexed": true,
    "internalType": "address",
    "name": "to",
    "type": "address"
  }, {"indexed": false, "internalType": "uint256", "name": "value", "type": "uint256"}],
  "name": "Transfer",
  "type": "event"
}, {
  "inputs": [],
  "name": "DEAD",
  "outputs": [{"internalType": "address", "name": "", "type": "address"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [],
  "name": "_hasLiqBeenAdded",
  "outputs": [{"internalType": "bool", "name": "", "type": "bool"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [],
  "name": "_ratios",
  "outputs": [{"internalType": "uint16", "name": "bank", "type": "uint16"}, {
    "internalType": "uint16",
    "name": "marketing",
    "type": "uint16"
  }, {"internalType": "uint16", "name": "totalSwap", "type": "uint16"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [],
  "name": "_taxRates",
  "outputs": [{"internalType": "uint16", "name": "buyFee", "type": "uint16"}, {
    "internalType": "uint16",
    "name": "sellFee",
    "type": "uint16"
  }, {"internalType": "uint16", "name": "transferFee", "type": "uint16"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [],
  "name": "_taxWallets",
  "outputs": [{
    "internalType": "address payable",
    "name": "marketing",
    "type": "address"
  }, {"internalType": "address payable", "name": "bank", "type": "address"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [{"internalType": "address", "name": "holder", "type": "address"}, {
    "internalType": "address",
    "name": "spender",
    "type": "address"
  }],
  "name": "allowance",
  "outputs": [{"internalType": "uint256", "name": "", "type": "uint256"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [{"internalType": "address", "name": "spender", "type": "address"}, {
    "internalType": "uint256",
    "name": "amount",
    "type": "uint256"
  }],
  "name": "approve",
  "outputs": [{"internalType": "bool", "name": "", "type": "bool"}],
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "inputs": [],
  "name": "approveContractContingency",
  "outputs": [{"internalType": "bool", "name": "", "type": "bool"}],
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "inputs": [{"internalType": "address", "name": "account", "type": "address"}],
  "name": "balanceOf",
  "outputs": [{"internalType": "uint256", "name": "", "type": "uint256"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [],
  "name": "contractSwapEnabled",
  "outputs": [{"internalType": "bool", "name": "", "type": "bool"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [],
  "name": "decimals",
  "outputs": [{"internalType": "uint8", "name": "", "type": "uint8"}],
  "stateMutability": "pure",
  "type": "function"
}, {
  "inputs": [],
  "name": "dexRouter",
  "outputs": [{"internalType": "contract IRouter02", "name": "", "type": "address"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [],
  "name": "enableTrading",
  "outputs": [],
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "inputs": [],
  "name": "getCirculatingSupply",
  "outputs": [{"internalType": "uint256", "name": "", "type": "uint256"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [],
  "name": "getOwner",
  "outputs": [{"internalType": "address", "name": "", "type": "address"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [{"internalType": "uint256", "name": "priceImpactInHundreds", "type": "uint256"}],
  "name": "getTokenAmountAtPriceImpact",
  "outputs": [{"internalType": "uint256", "name": "", "type": "uint256"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [{"internalType": "address", "name": "account", "type": "address"}],
  "name": "isExcludedFromFees",
  "outputs": [{"internalType": "bool", "name": "", "type": "bool"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [{"internalType": "address", "name": "account", "type": "address"}],
  "name": "isExcludedFromProtection",
  "outputs": [{"internalType": "bool", "name": "", "type": "bool"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [],
  "name": "launchStamp",
  "outputs": [{"internalType": "uint256", "name": "", "type": "uint256"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [],
  "name": "lockTaxes",
  "outputs": [],
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "inputs": [],
  "name": "lpPair",
  "outputs": [{"internalType": "address", "name": "", "type": "address"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [],
  "name": "marketingWallet",
  "outputs": [{"internalType": "address payable", "name": "", "type": "address"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [],
  "name": "maxBuyTaxes",
  "outputs": [{"internalType": "uint256", "name": "", "type": "uint256"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [],
  "name": "maxSellTaxes",
  "outputs": [{"internalType": "uint256", "name": "", "type": "uint256"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [],
  "name": "maxTransferTaxes",
  "outputs": [{"internalType": "uint256", "name": "", "type": "uint256"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [{"internalType": "address[]", "name": "accounts", "type": "address[]"}, {
    "internalType": "uint256[]",
    "name": "amounts",
    "type": "uint256[]"
  }], "name": "multiSendTokens", "outputs": [], "stateMutability": "nonpayable", "type": "function"
}, {
  "inputs": [],
  "name": "name",
  "outputs": [{"internalType": "string", "name": "", "type": "string"}],
  "stateMutability": "pure",
  "type": "function"
}, {
  "inputs": [],
  "name": "piContractSwapsEnabled",
  "outputs": [{"internalType": "bool", "name": "", "type": "bool"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [],
  "name": "piSwapPercent",
  "outputs": [{"internalType": "uint256", "name": "", "type": "uint256"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [{"internalType": "address", "name": "account", "type": "address"}],
  "name": "removeSniper",
  "outputs": [],
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "inputs": [],
  "name": "renounceOwnership",
  "outputs": [],
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "inputs": [{"internalType": "bool", "name": "swapEnabled", "type": "bool"}, {
    "internalType": "bool",
    "name": "priceImpactSwapEnabled",
    "type": "bool"
  }], "name": "setContractSwapEnabled", "outputs": [], "stateMutability": "nonpayable", "type": "function"
}, {
  "inputs": [{"internalType": "address", "name": "account", "type": "address"}, {
    "internalType": "bool",
    "name": "enabled",
    "type": "bool"
  }], "name": "setExcludedFromFees", "outputs": [], "stateMutability": "nonpayable", "type": "function"
}, {
  "inputs": [{"internalType": "address", "name": "account", "type": "address"}, {
    "internalType": "bool",
    "name": "enabled",
    "type": "bool"
  }], "name": "setExcludedFromProtection", "outputs": [], "stateMutability": "nonpayable", "type": "function"
}, {
  "inputs": [{"internalType": "address", "name": "initializer", "type": "address"}],
  "name": "setInitializer",
  "outputs": [],
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "inputs": [{"internalType": "address", "name": "pair", "type": "address"}, {
    "internalType": "bool",
    "name": "enabled",
    "type": "bool"
  }], "name": "setLpPair", "outputs": [], "stateMutability": "nonpayable", "type": "function"
}, {
  "inputs": [{"internalType": "address", "name": "newRouter", "type": "address"}],
  "name": "setNewRouter",
  "outputs": [],
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "inputs": [{"internalType": "uint256", "name": "priceImpactSwapPercent", "type": "uint256"}],
  "name": "setPriceImpactSwapAmount",
  "outputs": [],
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "inputs": [{"internalType": "bool", "name": "_antiSnipe", "type": "bool"}, {
    "internalType": "bool",
    "name": "_antiBlock",
    "type": "bool"
  }], "name": "setProtectionSettings", "outputs": [], "stateMutability": "nonpayable", "type": "function"
}, {
  "inputs": [{"internalType": "uint16", "name": "bank", "type": "uint16"}, {
    "internalType": "uint16",
    "name": "marketing",
    "type": "uint16"
  }], "name": "setRatios", "outputs": [], "stateMutability": "nonpayable", "type": "function"
}, {
  "inputs": [{"internalType": "uint256", "name": "thresholdPercent", "type": "uint256"}, {
    "internalType": "uint256",
    "name": "thresholdDivisor",
    "type": "uint256"
  }, {"internalType": "uint256", "name": "amountPercent", "type": "uint256"}, {
    "internalType": "uint256",
    "name": "amountDivisor",
    "type": "uint256"
  }], "name": "setSwapSettings", "outputs": [], "stateMutability": "nonpayable", "type": "function"
}, {
  "inputs": [{"internalType": "uint16", "name": "buyFee", "type": "uint16"}, {
    "internalType": "uint16",
    "name": "sellFee",
    "type": "uint16"
  }, {"internalType": "uint16", "name": "transferFee", "type": "uint16"}],
  "name": "setTaxes",
  "outputs": [],
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "inputs": [{
    "internalType": "address payable",
    "name": "marketing",
    "type": "address"
  }, {"internalType": "address payable", "name": "bank", "type": "address"}],
  "name": "setWallets",
  "outputs": [],
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "inputs": [],
  "name": "swapAmount",
  "outputs": [{"internalType": "uint256", "name": "", "type": "uint256"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [],
  "name": "swapThreshold",
  "outputs": [{"internalType": "uint256", "name": "", "type": "uint256"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [],
  "name": "sweepContingency",
  "outputs": [],
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "inputs": [{"internalType": "address", "name": "token", "type": "address"}],
  "name": "sweepExternalTokens",
  "outputs": [],
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "inputs": [],
  "name": "symbol",
  "outputs": [{"internalType": "string", "name": "", "type": "string"}],
  "stateMutability": "pure",
  "type": "function"
}, {
  "inputs": [],
  "name": "taxesAreLocked",
  "outputs": [{"internalType": "bool", "name": "", "type": "bool"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [],
  "name": "totalSupply",
  "outputs": [{"internalType": "uint256", "name": "", "type": "uint256"}],
  "stateMutability": "pure",
  "type": "function"
}, {
  "inputs": [],
  "name": "tradingEnabled",
  "outputs": [{"internalType": "bool", "name": "", "type": "bool"}],
  "stateMutability": "view",
  "type": "function"
}, {
  "inputs": [{"internalType": "address", "name": "recipient", "type": "address"}, {
    "internalType": "uint256",
    "name": "amount",
    "type": "uint256"
  }],
  "name": "transfer",
  "outputs": [{"internalType": "bool", "name": "", "type": "bool"}],
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "inputs": [{"internalType": "address", "name": "sender", "type": "address"}, {
    "internalType": "address",
    "name": "recipient",
    "type": "address"
  }, {"internalType": "uint256", "name": "amount", "type": "uint256"}],
  "name": "transferFrom",
  "outputs": [{"internalType": "bool", "name": "", "type": "bool"}],
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "inputs": [{"internalType": "address", "name": "newOwner", "type": "address"}],
  "name": "transferOwner",
  "outputs": [],
  "stateMutability": "nonpayable",
  "type": "function"
}, {"stateMutability": "payable", "type": "receive"}];
const mainAirdropWalletAddress = "0x26F0D4117772625c303A8b3a01616de8b730B166";
const initialAirdropWalletBalance = 230900000000000000000;

const airdropContractAddress = '0xdA357BBC6238AB9491e94B3D73bB9987621f6074';
const abi = [
  {
    "inputs": [
      {
        "internalType": "contract IERC20",
        "name": "_token",
        "type": "address"
      },
      {
        "internalType": "bytes32[]",
        "name": "_roots",
        "type": "bytes32[]"
      }
    ],
    "stateMutability": "nonpayable",
    "type": "constructor"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "indexed": true,
        "internalType": "address",
        "name": "previousOwner",
        "type": "address"
      },
      {
        "indexed": true,
        "internalType": "address",
        "name": "newOwner",
        "type": "address"
      }
    ],
    "name": "OwnershipTransferred",
    "type": "event"
  },
  {
    "inputs": [
      {
        "internalType": "uint256",
        "name": "_rootIndex",
        "type": "uint256"
      },
      {
        "internalType": "bytes32[]",
        "name": "_merkleProof",
        "type": "bytes32[]"
      }
    ],
    "name": "checkValidity",
    "outputs": [
      {
        "internalType": "bool",
        "name": "",
        "type": "bool"
      }
    ],
    "stateMutability": "view",
    "type": "function"
  },
  {
    "inputs": [],
    "name": "owner",
    "outputs": [
      {
        "internalType": "address",
        "name": "",
        "type": "address"
      }
    ],
    "stateMutability": "view",
    "type": "function"
  },
  {
    "inputs": [
      {
        "internalType": "address",
        "name": "_to",
        "type": "address"
      },
      {
        "internalType": "uint256",
        "name": "_amount",
        "type": "uint256"
      }
    ],
    "name": "ownerTransferTokens",
    "outputs": [],
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "inputs": [
      {
        "internalType": "address",
        "name": "",
        "type": "address"
      }
    ],
    "name": "receivedTokens",
    "outputs": [
      {
        "internalType": "bool",
        "name": "",
        "type": "bool"
      }
    ],
    "stateMutability": "view",
    "type": "function"
  },
  {
    "inputs": [],
    "name": "renounceOwnership",
    "outputs": [],
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "inputs": [
      {
        "internalType": "uint256",
        "name": "",
        "type": "uint256"
      }
    ],
    "name": "roots",
    "outputs": [
      {
        "internalType": "bytes32",
        "name": "",
        "type": "bytes32"
      }
    ],
    "stateMutability": "view",
    "type": "function"
  },
  {
    "inputs": [
      {
        "internalType": "uint256",
        "name": "_rootIndex",
        "type": "uint256"
      },
      {
        "internalType": "bytes32[]",
        "name": "_merkleProof",
        "type": "bytes32[]"
      }
    ],
    "name": "sendTokens",
    "outputs": [],
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "inputs": [],
    "name": "token",
    "outputs": [
      {
        "internalType": "contract IERC20",
        "name": "",
        "type": "address"
      }
    ],
    "stateMutability": "view",
    "type": "function"
  },
  {
    "inputs": [],
    "name": "tokenAmount",
    "outputs": [
      {
        "internalType": "uint256",
        "name": "",
        "type": "uint256"
      }
    ],
    "stateMutability": "view",
    "type": "function"
  },
  {
    "inputs": [
      {
        "internalType": "address",
        "name": "newOwner",
        "type": "address"
      }
    ],
    "name": "transferOwnership",
    "outputs": [],
    "stateMutability": "nonpayable",
    "type": "function"
  }
];
let airdropContract;
let rootIndex = null;
let merkleProof = null;

const gasLimit = 15000000;
let web3;
let contract;
let tokenContract;
let userAddress;


$(document).ready(function () {
  // $('.staking-section').hide()
  const audio = document.getElementById("audio");
  audio.volume = 0.2;

  getAirdropProgress();
  if ($(location).attr('href').includes("staking")) {
    updateTotalStaked();
  }

  $('.airdrop-section').hide()
  $('#approveAmount').prop('disabled', true)
  $('#stakeAmount').prop('disabled', true)
  $('#unstakeAmount').prop('disabled', true)
  $('#rootIndex').prop('disabled', true)
  $('#merkleProof').prop('disabled', true)
  $('#airdropLoader').hide();
})

async function getAirdropProgress() {
  try {
    const tempWeb3 = new Web3("https://arb-mainnet.g.alchemy.com/v2/g1j01I6DwgP9j-vHI6FuMGVOkvOONXgf");
    tokenContract = new tempWeb3.eth.Contract(tokenContractABI, tokenContractAddress);
    const currentAirdropWalletBalance = await tokenContract.methods.balanceOf(mainAirdropWalletAddress).call();
    const airdropContractWalletBalance = await tokenContract.methods.balanceOf(airdropContractAddress).call();

    const percent = ((initialAirdropWalletBalance - currentAirdropWalletBalance - airdropContractWalletBalance) / initialAirdropWalletBalance) * 100;
    const percentage = percent.toFixed(2) + "%";
    $('#airdropProgressBar').width(percentage);
    $('#airdropProgressBar').text(percentage);
    $('#airdropText').text(percentage);
  } catch (error) {
    console.log(error);
  }

}

$('#connectWallet').click(async function () {
  if (window.ethereum) {
    web3 = new Web3(window.ethereum);
    try {
      await window.ethereum.enable();
      userAddress = (await web3.eth.getAccounts())[0];
      contract = new web3.eth.Contract(contractABI, contractAddress);
      tokenContract = new web3.eth.Contract(tokenContractABI, tokenContractAddress);
      airdropContract = new web3.eth.Contract(abi, airdropContractAddress);
      console.log("Wallet connected:", userAddress);
      console.log(userAddress.substring(0, 4) + "..." + userAddress.substring(userAddress.length - 4))
      $('#connectWalletText').text(userAddress.substring(0, 4) + "..." + userAddress.substring(userAddress.length - 4))
      await preInit();
    } catch (error) {
      console.error("Error connecting wallet:", error);
    }
  } else {
    alert("Please install MetaMask to use this dApp!");
  }
})

$('#showStakeBtn').click(function () {
  $('.staking-section').show()
})

async function preInit() {
  $('#airdropLoader').show();
  if (!$(location).attr('href').includes("staking")) {
    console.log("Checking Merkle Proof");
    await getMerkleProof();
  } else {
    await updateInWalletAmount();
    await updateMyStakedAmount();
    await updateTotalRealised();
    await updateUnrealised();
    await updateTotalEarningsClaimed();
  }
  await init();
}

async function init() {
  $('.airdrop-section').show()
  $('#approveAmount').prop('disabled', false)
  $('#stakeAmount').prop('disabled', false)
  $('#unstakeAmount').prop('disabled', false)
  $('#rootIndex').prop('disabled', false)
  $('#merkleProof').prop('disabled', false)
  $('#airdropLoader').hide();

}

async function getMerkleProof() {
  if (!userAddress) {
    return;
  }
  const response = await fetch(`https://merkle-proof-386615.uc.r.appspot.com/api/getProof?address=` + userAddress, {
    method: "GET",
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  });
  const data = await response.json();
  if (!data.error) {
    console.log(data);
    rootIndex = data.index;
    merkleProof = JSON.stringify(data.proof, null, 2);
  } else {
    $('#claimAirdropBtn').text('Not Eligible for Airdrop');
  }

}

$('#approveBtn').click(async function () {
  const value = $('#approveAmount').val();
  const amount = Number.parseInt(value);
  if (Number.isNaN(amount) || amount <= 0) {
    window.alert("Please enter correct amount");
    return;
  }
  const tokenAmount = web3.utils.toBN(amount).mul(web3.utils.toBN(10 ** tokenDecimals));
  try {
    const gasPrice = await web3.eth.getGasPrice();
    const gasEstimate = await tokenContract.methods.approve(contractAddress, tokenAmount.toString()).estimateGas({
      from: userAddress,
      gasPrice
    });
    const maxGasLimit = 150000000;
    const gasLimit = Math.min(maxGasLimit, gasEstimate);
    await tokenContract.methods.approve(contractAddress, tokenAmount.toString()).send({
      from: userAddress,
      gas: gasLimit,
      gasPrice
    });
    window.alert("Approval successful, Please stake now");
  } catch (error) {
    console.error("Error approving tokens:", error);
  }
})

$('#stakeBtn').click(async function () {
  if (!userAddress) {
    window.alert("Please connect your wallet first");
    return;
  }
  const value = $('#stakeAmount').val();
  const amount = Number.parseFloat(value);
  if (Number.isNaN(amount) || amount <= 0) {
    window.alert("Please enter correct amount");
    return;
  }
  const inWalletBalance = await tokenContract.methods.balanceOf(userAddress).call()

  const totalAmount = amount * (10 ** tokenDecimals);
  if (inWalletBalance < totalAmount) {
    return window.alert("Please buy enough MUMU first");
  }
  const tokenAmount = web3.utils.toBN(totalAmount);
  const allowed = await tokenContract.methods.allowance(userAddress, contractAddress).call();
  if (allowed < totalAmount) {
    console.log("Approve First");
    try {
      const gasPrice = await web3.eth.getGasPrice();
      const gasEstimate = await tokenContract.methods.approve(contractAddress, tokenAmount.toString()).estimateGas({
        from: userAddress,
        gasPrice
      });
      const maxGasLimit = 150000000;
      const gasLimit = Math.min(maxGasLimit, gasEstimate);
      await tokenContract.methods.approve(contractAddress, tokenAmount.toString()).send({
        from: userAddress,
        gas: gasLimit,
        gasPrice
      });
      console.log("Approved");
    } catch (error) {
      window.alert("Approval Failed, Please try again");
      console.log(error);
      return;
    }
  }

  try {
    const gasPrice = await web3.eth.getGasPrice();
    const gasEstimate = await contract.methods.stake(tokenAmount.toString()).estimateGas({
      from: userAddress,
      gasPrice
    });
    const maxGasLimit = 150000000;
    const gasLimit = Math.min(maxGasLimit, gasEstimate);
    await contract.methods.stake(tokenAmount.toString()).send({from: userAddress, gas: gasLimit, gasPrice});
    window.alert("Stake Successful");
  } catch (error) {
    console.error("Error staking tokens:", error);
  }
});

$('#unstakeBtn').click(async function () {
  if (!userAddress) {
    window.alert("Please connect your wallet first");
    return;
  }
  const value = $('#unstakeAmount').val();
  const amount = Number.parseInt(value);
  if (Number.isNaN(amount) || amount <= 0) {
    window.alert("Please enter correct amount");
    return;
  }
  const tokenAmount = web3.utils.toBN(amount).mul(web3.utils.toBN(10 ** tokenDecimals));
  try {
    const gasPrice = await web3.eth.getGasPrice();
    const gasEstimate = await contract.methods.unstake(tokenAmount.toString()).estimateGas({
      from: userAddress,
      gasPrice
    });
    const maxGasLimit = 150000000;
    const gasLimit = Math.min(maxGasLimit, gasEstimate);
    await contract.methods.unstake(tokenAmount.toString()).send({from: userAddress, gas: gasLimit, gasPrice});
    window.alert("UnStake Successful");
  } catch (error) {
    console.error("Error unstaking tokens:", error);
  }
});

$('#claimBtn').click(async function () {
  if (!userAddress) {
    window.alert("Please connect your wallet first");
    return;
  }
  try {
    const gasPrice = await web3.eth.getGasPrice();
    const gasEstimate = await contract.methods.realise().estimateGas({from: userAddress, gasPrice});
    const maxGasLimit = 150000000;
    const gasLimit = Math.min(maxGasLimit, gasEstimate);
    await contract.methods.realise().send({from: userAddress, gas: gasLimit, gasPrice});
    window.alert("Rewards Claim Successful");
  } catch (error) {
    console.error("Error claiming rewards:", error);
  }
});

$('#getStakedAmtBtn').click(async function () {
  try {
    const stakedAmount = await contract.methods.getStakedAmount(userAddress).call();
    $('#myStakeValue').text(Number(stakedAmount / (10 ** tokenDecimals)).toLocaleString());
    console.log("Staked tokens fetched successfully");
  } catch (error) {
    console.error("Error fetching staked tokens:", error);
  }
});

$('#stakeAllBtn').click(async function () {
  try {
    const gasPrice = await web3.eth.getGasPrice();
    const gasEstimate = await contract.methods.stakeAll().estimateGas({from: userAddress, gasPrice});
    const maxGasLimit = 150000000;
    const gasLimit = Math.min(maxGasLimit, gasEstimate);
    await contract.methods.stakeAll().send({from: userAddress, gas: gasLimit, gasPrice});
    window.alert("Stake All Successful");
  } catch (error) {
    console.error("Error staking all tokens:", error);
  }
});

$('#unstakeAllBtn').click(async function () {
  try {
    const gasPrice = await web3.eth.getGasPrice();
    const gasEstimate = await contract.methods.unstakeAll().estimateGas({from: userAddress, gasPrice});
    const maxGasLimit = 2000000;
    const gasLimit = Math.min(maxGasLimit, gasEstimate);
    await contract.methods.unstakeAll().send({from: userAddress, gas: gasLimit, gasPrice});
    window.alert("UnStake All Successful");

  } catch (error) {
    console.error("Error unstaking all tokens:", error);
  }
});

async function updateTotalStaked() {
  try {
    if (window.ethereum) {
      const tempWeb3 = new Web3("https://arb-mainnet.g.alchemy.com/v2/g1j01I6DwgP9j-vHI6FuMGVOkvOONXgf");
      contract = new tempWeb3.eth.Contract(contractABI, contractAddress);
      const totalStaked = await contract.methods.totalStaked().call();
      const formattedTotalStaked = (totalStaked / (10 ** tokenDecimals)).toFixed(0);
      console.log("Total Staked:", formattedTotalStaked);
      $('#totalStakedValue').text(Number(formattedTotalStaked).toLocaleString('en'));
      const percent = (formattedTotalStaked / 2324000000000) * 100;
      const percentage = percent.toFixed(2) + "%";
      $('#stakingProgressDiv').width(percentage);
      $('#stakingProgressDiv').text(percentage);
      // $('#airdropText').text(percentage);
    }
  } catch (error) {
    console.error("Error getting total staked:", error);
  }
}

async function updateTotalRealised() {
  try {
    const totalRealised = await contract.methods.totalRealised().call();
    const formattedTotalRealised = (totalRealised / (10 ** 18)).toFixed(9);
    console.log("Total Realised:", formattedTotalRealised.toLocaleString("en"));
    $('#totalRealized').text(Number(formattedTotalRealised).toLocaleString())
    console.log("Unrealised: " + await contract.methods.getUnrealisedEarnings(userAddress).call())
  } catch (error) {
    console.error("Error getting total realised:", error);
  }
}

async function updateTotalEarningsClaimed() {
  try {
    const accounts = await web3.eth.getAccounts();
    const connectedWallet = accounts[0];

    const totalEarningsClaimed = await contract.methods.getRealisedEarnings(connectedWallet).call();
    const formattedTotalEarningsClaimed = (totalEarningsClaimed / (10 ** 18)).toFixed(9);
    $('#earnedValue').text(formattedTotalEarningsClaimed)
  } catch (error) {
    console.error("Error getting total Earned:", error);
  }
}

async function updateUnrealised() {
  try {
    const unrealised = await contract.methods.getUnrealisedEarnings(userAddress).call();
    const formattedunrealised = (unrealised / (10 ** 18)).toFixed(9);
    $('#unrealisedValue').text(formattedunrealised)
  } catch (error) {
    console.error("Error getting total realised:", error);
  }
}

async function updateInWalletAmount() {
  try {
    const tokens = await tokenContract.methods.balanceOf(userAddress).call()
    const formattedTokens = (tokens / (10 ** tokenDecimals)).toFixed(2);
    $('#inWalletValue').text(formattedTokens);
  } catch (error) {
    console.error("Error fetching in wallet tokens:", error);
    console.log(error)
  }
}

async function updateMyStakedAmount() {
  try {
    const stakedAmount = await contract.methods.getStakedAmount(userAddress).call();
    const formattedStakedAmount = (stakedAmount / (10 ** tokenDecimals)).toFixed(0);
    $('#myStakeValue').text(Number(formattedStakedAmount).toLocaleString("en"));
    console.log("Staked tokens fetched successfully");
  } catch (error) {
    console.error("Error fetching staked tokens:", error);
  }
}

$('#getMaxStakeBtn').click(async function () {
  if (!userAddress) {
    window.alert("Please connect your wallet first");
    return;
  }
  const tokens = await tokenContract.methods.balanceOf(userAddress).call()
  const formattedTokens = Math.floor(tokens / (10 ** tokenDecimals));
  if (formattedTokens < 0.01) {
    $('#stakeAmount').val(0);
    return;
  }
  $('#stakeAmount').val(formattedTokens);
})

$('#getMaxUnstakeBtn').click(async function () {
  if (!userAddress) {
    window.alert("Please connect your wallet first");
    return;
  }
  const tokens = await contract.methods.getStakedAmount(userAddress).call();
  const formattedTokens = (tokens / (10 ** tokenDecimals))
  $('#unstakeAmount').val(formattedTokens);
})

$('#claimAirdropBtn').click(async function () {
  if (rootIndex === null || !merkleProof) {
    alert("Incorrect root index or proof");
    return;
  }
  const formattedMerkleProof = merkleProof.startsWith('[') ? merkleProof.slice(1, -1) : merkleProof;
  const merkleProofArray = JSON.parse('[' + formattedMerkleProof + ']');
  try {
    console.log("Trying gas estimate")
    const gasPrice = await web3.eth.getGasPrice();
    const gasEstimate = await airdropContract.methods.sendTokens(rootIndex, merkleProofArray).estimateGas({
      from: userAddress,
      gasPrice
    });
    const maxGasLimit = 150000000;
    const gasLimit = Math.min(maxGasLimit, gasEstimate);
    await airdropContract.methods.sendTokens(rootIndex, merkleProofArray).send({
      from: userAddress,
      gas: gasLimit,
      gasPrice
    });
    alert('Tokens sent successfully');
  } catch (error) {
    console.log(error);
    $('#claimAirdropError').text(error.message);
    if (error.message.includes("Tokens have already been claimed")) {
      alert("You have already claimed tokens.");
    } else {
      console.error('Error sending tokens:', error);
      alert(error);
    }
  }
})

$('#addToMetamask').click(async function () {
  if (window.ethereum) {
    await ethereum.request({
      method: 'wallet_watchAsset',
      params: {
        type: 'ERC20',
        options: {
          address: tokenContractAddress,
          symbol: 'MUMU',
          decimals: tokenDecimals,
          image: 'https://www.mumuthebull.io/assets/logo.png'
        }
      }
    })
  }
})

